package com.example.agenda_arraylist;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {

    TableLayout tblLista;
    ArrayList<Contacto> contactos;
    ArrayList<Contacto>filter;



    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);



        tblLista = (TableLayout) findViewById(R.id.tblLista);
        Bundle bundleObject = getIntent().getExtras();
        contactos = (ArrayList<Contacto>) bundleObject.getSerializable("contactos");
        filter = contactos;
        Button btnNuevo = (Button) findViewById(R.id.btnNuevo);
        btnNuevo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
        cargarContactos();



    }

    public void cargarContactos() {
        for(int x=0; x < contactos.size();x++) {
            final Contacto contacto = new Contacto(contactos.get(x));
            TableRow nRow=new TableRow(ListActivity.this);
            TextView nText=new TextView(ListActivity.this);
            nText.setText(contacto.getNombre());
            nText.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            nText.setTextColor((contacto.isFavorito())? Color.BLUE:Color.BLACK);
            nRow.addView(nText);
            Button nButton = new Button(ListActivity.this);
            nButton.setText(R.string.acver);
            nButton.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            nButton.setTextColor(Color.BLACK);





            nButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Contacto c=(Contacto)v.getTag(R.string.contacto_g);
                    Intent i= new Intent();
                    Bundle oBundle = new Bundle();
                    oBundle.putSerializable("contacto",c);
                    oBundle.putInt("index",Integer.valueOf(v.getTag(R.string.contacto_g_index).toString()));
                    i.putExtras(oBundle);setResult(RESULT_OK,i);
                    finish();
                }
            });
            Button btnEliminar = new Button(ListActivity.this);
            btnEliminar.setText("Eliminar");
            btnEliminar.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            btnEliminar.setTextColor(Color.BLACK);
            btnEliminar.setOnClickListener(this.btnEliminarsAction(contacto.getID()));
            btnEliminar.setTag(R.string.contacto_g_index, contacto.getID());
            nRow.addView(btnEliminar);

            nButton.setTag(R.string.contacto_g,contacto);
            nButton.setTag(R.string.contacto_g_index,x);
            nRow.addView(nButton);
            tblLista.addView(nRow);
        }
    }
    public View.OnClickListener btnEliminarsAction(final long id) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("ID", String.valueOf(id));
                deleteContacto(id);
            }
        };
    }
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.searchview,menu);
        MenuItem menuItem = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView)menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String sr) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String sr) {
                Buscar(sr);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);}
        public void Buscar(String sr){
        ArrayList<Contacto>list=new ArrayList<>();
        for (int x=0; x<filter.size(); x++){
            if (filter.get(x).getNombre().contains(sr))list.add(filter.get(x));
        }
        contactos = list;
        tblLista.removeAllViews();
        cargarContactos();
        }

    private void deleteContacto(long id) {
        for(int x = 0; x < filter.size(); x++) {
            if(filter.get(x).getID() == id) {
               this.filter.remove(x);
                Toast.makeText(this, "Se eliminó el contacto",Toast.LENGTH_SHORT).show();
                break;
            }
        }

        this.contactos = filter;
        tblLista.removeAllViews();
        cargarContactos();
    }

}
