package com.example.agenda_arraylist;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    final ArrayList<Contacto> contactos = new ArrayList<Contacto>();
    EditText txtNombre;
    EditText txtTel;
    EditText txtTelefono2;
    EditText txtDomicilio;
    EditText txtNotas;
    CheckBox cbxFavorito;
    Contacto saveContact;
    Button   btnSalir;

    int savedIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtTel = (EditText) findViewById(R.id.txtTel1);
        txtTelefono2 = (EditText) findViewById(R.id.txtTel2);
        txtDomicilio = (EditText) findViewById(R.id.txtDomicilio);
        txtNotas = (EditText) findViewById(R.id.txtNotas);
        cbxFavorito = (CheckBox) findViewById(R.id.chkFavorito);
        Button btnGuardar = (Button) findViewById(R.id.btnGuardar);
        Button btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        Button btnListar = (Button) findViewById(R.id.btnListar);
        Button btnSalir = (Button) findViewById(R.id.btnSalir);

        saveContact = null;
        savedIndex = 0;

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtNombre.getText().toString().equals("") || txtDomicilio.getText().toString().equals("") || txtTel.getText().toString().equals("")) {
                    Toast.makeText(MainActivity.this, R.string.msgerror, Toast.LENGTH_SHORT).show();
                } else {
                    Contacto nContacto = new Contacto();
                    int index = contactos.size();
                    if (saveContact != null) {
                        contactos.remove(savedIndex);
                        nContacto = saveContact;
                        index = savedIndex;
                    }

                    nContacto.setNombre(txtNombre.getText().toString());
                    nContacto.setTelefono1(txtTel.getText().toString());
                    nContacto.setTelefono2(txtTelefono2.getText().toString());
                    nContacto.setDomicilio(txtDomicilio.getText().toString());
                    nContacto.setNotas(txtNotas.getText().toString());
                    nContacto.setFavorito(cbxFavorito.isChecked());
                    contactos.add(index, nContacto);
                    Toast.makeText(MainActivity.this, R.string.mensaje, Toast.LENGTH_SHORT).show();
                    saveContact = null;
                    limpiar();
                }
            }
        });

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListActivity.class);
                startActivity(intent);
            }
        });

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ListActivity.class);
                Bundle bObject = new Bundle();
                bObject.putSerializable("contactos", contactos);
                i.putExtras(bObject);
                startActivityForResult(i, 0);
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtNombre.getText().toString().equals("") || txtDomicilio.getText().toString().equals("") || txtTel.getText().toString().equals("")) {
                    Toast.makeText(MainActivity.this, R.string.msgerror, Toast.LENGTH_SHORT).show();
                } else {
                    Contacto nContacto = new Contacto();
                    int index = contactos.size();
                    if (saveContact != null) {
                        contactos.remove(savedIndex);
                        nContacto = saveContact;
                        index = savedIndex;
                    }
                    nContacto.setNombre(txtNombre.getText().toString());
                    nContacto.setTelefono1(txtTel.getText().toString());
                    nContacto.setTelefono2(txtTelefono2.getText().toString());
                    nContacto.setDomicilio(txtDomicilio.getText().toString());
                    nContacto.setNotas(txtNotas.getText().toString());
                    nContacto.setFavorito(cbxFavorito.isChecked());
                    contactos.add(index, nContacto);
                    Toast.makeText(MainActivity.this, R.string.mensaje, Toast.LENGTH_SHORT).show();
                    saveContact = null;
                    saveContact = null;
                    limpiar();
                }
            }
        });

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ListActivity.class);
                Bundle bObject = new Bundle();
                bObject.putSerializable("contactos", contactos);
                i.putExtras(bObject);
                startActivityForResult(i, 0);
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (intent != null) {
            Bundle oBundle = intent.getExtras();
            saveContact = (Contacto) oBundle.getSerializable("contacto");
            savedIndex = oBundle.getInt("index");
            txtNombre.setText(saveContact.getNombre());
            txtTel.setText(saveContact.getTelefono1());
            txtTelefono2.setText(saveContact.getTelefono2());
            txtDomicilio.setText(saveContact.getDomicilio());
            txtNotas.setText(saveContact.getNotas());
            cbxFavorito.setChecked(saveContact.isFavorito());
        } else {
            limpiar();
        }
    }

    public void limpiar() {
        saveContact = null;
        txtNombre.setText("");
        txtTel.setText("");
        txtTelefono2.setText("");
        txtDomicilio.setText("");
        txtNotas.setText("");
        cbxFavorito.setChecked(false);
    }

    }

